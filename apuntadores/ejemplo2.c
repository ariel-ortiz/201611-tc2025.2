#include <stdio.h>

size_t mi_strlen(const char *s) {
    size_t cont = 0;
    while (*s++) cont++;
    return cont;
}

void reversa(char *s) {
    char *p = s;
    while (*p++);
    p -= 2;
    while (s < p) {
        char t = *s;
        *s = *p;
        *p = t;
        s++;
        p--;
    }
}

void *mi_memset(void *s, int c, size_t n) {
    char *p = s;
    while (n--) {
        *p++ = c;
    }
    return s;
}

int main(void) {
    printf("%zu\n", mi_strlen("Hola"));
    
    char cadena[] = "Hola Mundo!";
    printf("%s\n", cadena);
    reversa(cadena);
    printf("%s\n", cadena);
    
    int x[5] = {1, 2, 3, 4, 5};
    mi_memset(x, 1, sizeof(x));
    int i;
    for (i = 0; i < 5; i++) {
        printf("%x ", x[i]);
    }
    
    printf("\n");
    
    return 0;
}