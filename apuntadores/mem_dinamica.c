/*
 * Para compilar:
 *                 gcc -g mem_dinamica.c -o mem_dinamica
 *
 * Para correr:
 *                 valgrind --leak-check=full ./mem_dinamica
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int TAMANO = 100;

int main(void) {
    char* p = malloc(TAMANO);
    if (!p) {
        fprintf(stderr, "Memoria insuficiente.");
        exit(1);
    }
    memset(p, 0, TAMANO);
    // hacer algo con la memoria.
    
    // Deliberadamente no llamamos a free.
    // free(p);
    return 0;
}
