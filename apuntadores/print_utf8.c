#include <stdio.h>

int main(void) {
    printf("I❤U\n");
    
    char glyph[4];
    glyph[0] = 0xe2;
    glyph[1] = 0x9d;
    glyph[2] = 0xa4;
    glyph[3] = '\0';
    printf("%s\n", glyph);
    return 0;
}