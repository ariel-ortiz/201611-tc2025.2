#include <stdio.h>

int suma(int a, int b) {
    return a + b;
}

int resta(int a, int b) {
    return a - b;
}

int main(void) {
    int (*f)(int, int);
    f = suma;
    int x;
    x = f(2, 3);
    printf("%d\n", x); // 5
    f = &resta;
    x = (*f)(2, 3);
    printf("%d\n", x); // -1
    
    return 0;
}
