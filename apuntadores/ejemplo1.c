#include <stdio.h>
#include <stdlib.h>

int x;
int y = 5;
int z;

int main(void) {
    char c = 'A';
    char * p = &c;
    printf("c = %c\n", c);
    printf("p = %p\n", p);
    printf("*p = %c\n", *p);
    (*p)++;
    printf("c = %d\n", c);
    
    printf("&x = %p\n", &x);
    printf("&y = %p\n", &y);
    printf("&z = %p\n", &z);
    printf("&main = %p\n", &main);
    
    void* pnt = malloc(10);
    printf("pnt = %p\n", pnt);
    free(pnt);
    pnt = NULL;
    
    return 0;
}