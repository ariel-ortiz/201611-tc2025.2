// Nombre: 
// Matrícula:

#include <stdio.h>
#include <string.h>

void * lsearch(const void * key, const void * base,
               size_t nelem, size_t size,
               int (* cmp)(const void *, const void *)) {    
    const char *inicio = base;
    size_t i;
    for (i = 0; i < nelem; i++) {
        if (!cmp(key, inicio + i * size)) {
            return (void *)(inicio + i * size);
        }
    }
    return NULL;
}

int cmpi(const void * p1, const void * p2) {
    return * (const int *) p1 - * (const int *) p2;
}

int cmps(const void * p1, const void * p2) {
    return strcmp(* (char **) p1, * (char **) p2);
}

int main(void) {
    int ai[] = {23, 8, 42, 4, 16, 15};
    size_t size_ai = sizeof(ai) / sizeof(ai[0]);
    int key_ai = 42;
    int * pi = lsearch(&key_ai, ai, size_ai, sizeof(ai[0]), cmpi);

    if (pi) {
        printf("Se encontró %d en el índice %ld.\n", key_ai, pi - ai);
    } else {
        printf("No se encontró %d.\n", key_ai);
    }

    key_ai = 9;
    pi = lsearch(&key_ai, ai, size_ai, sizeof(ai[0]), cmpi);

    if (pi) {
        printf("Se encontró %d en el índice %ld.\n", key_ai, pi - ai);
    } else {
        printf("No se encontró %d.\n", key_ai);
    }

    char * as[] = {"Dwalin", "Balin", "Kili", "Fili", "Dori",
                   "Nori", "Ori", "Oin", "Gloin", "Bifur",
                   "Bofur", "Bombur", "Thorin"};
    size_t size_as = sizeof(as) / sizeof(as[0]);
    char * key_as = "Thorin";
    char ** ps = lsearch(&key_as, as, size_as, sizeof(as[0]), cmps);

    if (ps) {
        printf("Se encontró \"%s\" en el índice %ld.\n", key_as, ps - as);
    } else {
        printf("No se encontró \"%s\".\n", key_as);
    }

    key_as = "Tyrion";
    ps = lsearch(&key_as, as, size_as, sizeof(as[0]), cmps);

    if (ps) {
        printf("Se encontró \"%s\" en el índice %ld.\n", key_as, ps - as);
    } else {
        printf("No se encontró \"%s\".\n", key_as);
    }

    return 0;
}