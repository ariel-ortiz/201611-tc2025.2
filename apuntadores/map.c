#include <stdio.h>

void map(int (*f)(int), int* data, size_t n) {
    int i;
    for (i = 0; i < n; i++) {
        data[i] = f(data[i]);
    }
}

void imprime(int* data, size_t n) {
    int i;
    for (i = 0; i < n; i++) {
        printf("%d ", data[i]);
    }
    printf("\n");
}

int sex(int x) {
    return -x;
}

int sq(int x) {
    return x * x;
}

int zero(int x) {
    return 0;
}

int main(void) {
    int a[] = {2, 3, -1, 5, 10};
    size_t n = sizeof(a) / sizeof(a[0]);
    imprime(a, n);
    map(sex, a, n);
    imprime(a, n);
    map(sq, a, n);
    imprime(a, n);
    map(zero, a, n);
    imprime(a, n);
    return 0;
}