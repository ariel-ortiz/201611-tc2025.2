#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct _alumno {
    char nombre[30];
    int matricula;
    double promedio;
};

typedef struct _alumno alumno_t;

int cmp_int(const void *e1, const void *e2) {
    const int * i1 = e1;
    const int * i2 = e2;
    return *i1 - *i2;
}

int cmp_alumno(const void *e1, const void *e2) {
    const alumno_t * a1 = e1;
    const alumno_t * a2 = e2;
    return strcmp(a1->nombre, a2->nombre);
}

int main(void) {
    int a1[] = {4, 7, -2, 8, 5, 0, 2, 7, -1, 0, 9, 2, 4};
    size_t t1 = sizeof(a1) / sizeof(a1[0]);
    int i;
    
    for (i = 0; i < t1; i++) {
        printf("%d ", a1[i]);
    }
    printf("\n");
    
    qsort(a1, t1, sizeof(a1[0]), cmp_int);
    
    for (i = 0; i < t1; i++) {
        printf("%d ", a1[i]);
    }
    printf("\n");
    
    alumno_t a2[] = {
        {"Juan", 123, 78.1}, 
        {"Maria", 199, 89.3},
        {"Pedro", 145, 69.9},
        {"Conchita", 178, 85.2},
        {"Damian", 666, 66.6}
    };
    size_t t2 = sizeof(a2) / sizeof(a2[0]);
    
    for (i = 0; i < t2; i++) {
        printf("%d %s %f\n", a2[i].matricula, a2[i].nombre, a2[i].promedio);
    }
    printf("\n");
    
    qsort(a2, t2, sizeof(a2[0]), cmp_alumno);
    
    for (i = 0; i < t2; i++) {
        printf("%d %s %f\n", a2[i].matricula, a2[i].nombre, a2[i].promedio);
    }
    printf("\n");
    
    return 0;
}
