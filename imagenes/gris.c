#include <stdio.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

enum RGB {R, G, B};

void convierte(unsigned char *data, int x, int y) {
    int total = x * y;
    int i;
    for (i = 0; i < total; i++) {
        unsigned char *pixel = data + i * 3;
        int promedio = (pixel[R] + pixel[G] + pixel[B]) / 3;
        pixel[R] = pixel[G] = pixel[B] = promedio;
    }
}

int main(void) {
    int x,y,n;
    unsigned char *data = stbi_load("scarlett.png", &x, &y, &n, 0);
    if (data) {
        printf("x = %d\n", x);
        printf("y = %d\n", y);
        printf("n = %d\n", n);
        convierte(data, x, y);
        if (!stbi_write_png("scargris.png", x, y, n, data, x * n)) {
            fprintf(stderr, "Error al escribir archivo.\n");
        }
        stbi_image_free(data);
    }
    return 0;
}