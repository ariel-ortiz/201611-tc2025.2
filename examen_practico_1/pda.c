/*=========================================================

    Coloca aquí tu matrícula y nombre.
    
===========================================================*/

#include <stdio.h>

int pda(unsigned x) {
    
    if (x == 0) {
        return 1;
    }
    
    unsigned sigma = 0;
    unsigned i;
    
    for (i = 1; i <= x / 2; i++) {
        if (x % i == 0) {
            sigma += i;
        }
    }
    
    if (x == sigma) {
        return 0;
    }
    
    if (sigma > x) {
        return 1;
    }
    
    return -1;
}

int main(void) {
    printf("%2d\n", pda(0));
    printf("%2d\n", pda(6));
    printf("%2d\n", pda(10));
    printf("%2d\n", pda(12));
    printf("%2d\n", pda(8128));
    printf("%2d\n", pda(8129));
    printf("%2d\n", pda(33550335));
    printf("%2d\n", pda(33550336));
    printf("%2d\n", pda(33550337));
    return 0;    
}
