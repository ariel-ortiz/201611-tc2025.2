/*=========================================================

    Coloca aquí tu matrícula y nombre.
    
===========================================================*/

#include <stdio.h>
#include <stdint.h>

uint32_t empaca(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {
    return a | (b << 8) | (c << 16) | (d << 24);
}

int main(void) {
    printf("0x%08X\n", empaca(0x78, 0x56, 0x34, 0x12));
    printf("0x%08X\n", empaca(1, 1, 1, 1));
    printf("0x%08X\n", empaca(255, 255, 255, 255));
    printf("0x%08X\n", empaca(1, 2, 3, 4));
    return 0;
}