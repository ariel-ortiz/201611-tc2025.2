/*=========================================================

    Coloca aquí tu matrícula y nombre.
    
===========================================================*/

#include <stdio.h>
#include <stdarg.h>
#include <math.h>

double max(double x, ...) {
    
    if (x == -INFINITY) {
        return x;
    }
    
    double mayor = x;
    double actual;
    
    va_list lst;
    
    va_start(lst, x);
    
    while((actual = va_arg(lst, double)) != -INFINITY) {
        if (actual > mayor) {
            mayor = actual;
        }
    }
    
    va_end(lst);
    
    return mayor;
}

int main(void) {
    printf("%6.2f\n", max(-35.12, 19.78, 90.49, 67.59, -90.12, 
        57.66, 39.56,  80.78, 6.49, -5.30, -68.39, 82.49, 
        -62.62, 81.26, -35.47, -INFINITY));
    printf("%6.2f\n", max(-26.68, -86.04, -22.96, -32.80, -59.83, 
        -91.43, -99.69, -28.83, -57.25, -5.98, -INFINITY));
    printf("%6.2f\n", max(42.0, -INFINITY));
    printf("%6.2f\n", max(-INFINITY));
    return 0;
}