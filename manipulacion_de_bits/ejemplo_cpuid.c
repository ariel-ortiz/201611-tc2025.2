#include <stdio.h>
#include <stdint.h>
#include "cpuid.h"

int main(void) {
    uint32_t a, b, c, d;
    cpuid(1, a, b, c, d);
    if (d & 1) {
        printf("Si hay FPU\n");
    }
    
    cpuid(0x80000008, a, b, c, d);
    uint32_t mem_fis = a & 0xFF;
    uint32_t mem_vir = (a >> 8) & 0xFF;
    printf("Número de bits de memoria física = %d\n", mem_fis);
    printf("Número de bits de memoria virtual = %d\n", mem_vir);
    
    return 0;
}