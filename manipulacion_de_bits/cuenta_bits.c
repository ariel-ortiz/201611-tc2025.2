#include <stdio.h>

// Devuelve el número de bits 1 en el número x.
int cuenta_bits(unsigned x) {
    int bits = 0;
    while (x) {
        bits += (x & 1);
        x >>= 1;
    }
    return bits;
}

int main(void) {
    printf("%d\n", cuenta_bits(5));   // 2
    printf("%d\n", cuenta_bits(15));  // 4
    printf("%d\n", cuenta_bits(-1));  // 32
    printf("%d\n", cuenta_bits(256)); // 1
    printf("%d\n", cuenta_bits(255)); // 8
    return 0;
}
