#include <stdio.h>

int main(void) {
    
    // NOT
    unsigned char c = 0xA3;
    c = ~c;
    printf("%02x\n", c);
    
    // AND
    unsigned char a = 0x2D, b = 0xF3;
    printf("%02x\n", a & b);
    
    // OR
    printf("%02x\n", a | b);
    
    // XOR
    printf("%02x\n", a ^ b);
    
    unsigned char c1 = -10;
    signed char c2 = -10;
    
    c1 >>= 2;
    c2 >>= 2;
    
    printf("%02x\n", (unsigned char) c1);
    printf("%02x\n", (unsigned char) c2);
    
    return 0;
}