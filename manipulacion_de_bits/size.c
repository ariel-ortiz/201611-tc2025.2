/*
   Este programa determina el tamaño
   de ciertos tipos de datos en la
   plataforma anfitriona.
 */
 
 #include <stdio.h>
 
int main(void) {
    // Debe ser 1 siempre.
    printf("sizeof(char) = %zu\n", sizeof(char));
    
    printf("sizeof(short) = %zu\n", sizeof(short));
    printf("sizeof(int) = %zu\n", sizeof(int));
    printf("sizeof(long) = %zu\n", sizeof(long));
    printf("sizeof(long long) = %zu\n", sizeof(long long));
    
    printf("Número de bits por byte = %d\n", __CHAR_BIT__);
    
    unsigned char c = 1;
    int bits = 0;
    while (c) {
        c <<= 1;
        bits++;
    }
    printf("Número de bits por byte (calculado) = %d\n", bits);
    
    return 0;
 }