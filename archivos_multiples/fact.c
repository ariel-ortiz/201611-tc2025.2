#include "fact.h"

int fact(int n) {
    int i;
    int r = 1;
    for (i = 2; i <= n; i++) {
        r *= i;
    }
    return r;
}