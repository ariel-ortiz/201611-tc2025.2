#include <stdio.h>
#include "fibo.h"
#include "fact.h"

int main(void) {
    printf("%d\n", MI_CONSTANTE);
    printf("fibo(5) = %d\n", fibo(5));
    printf("fact(5) = %d\n", fact(5));
    printf("Bye bye!\n");
    return 0;
}