#include "fibo.h"

int fibo(int n) {
    int a, b, i;
    a = 0;
    b = 1;
    for (i = 0; i < n; i++) {
        int t = a;
        a = b;
        b = b + t;
    }
    return a;
}