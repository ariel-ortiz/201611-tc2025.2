/*
 * Primer programa ejemplo en lenguaje C.
 *
 * Para compilar:
 *                  gcc -o hola hola.c
 */

#include <stdio.h>

int main(void) {
    printf("¡Hola mundo!\n");
    printf("¡Adios Mundo Cruel!\n");
    return 0;
}