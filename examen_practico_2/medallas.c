//==========================================================
// Solución al problema 1.
//==========================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NOMBRE_LEN 15

typedef struct pais {
    char nombre[NOMBRE_LEN];
    int oro;
    int plata;
    int bronce;
} pais_t;

pais_t medallero[] = {                
    {"Mexico", 1, 3, 3},
    {"Kuwait", 0, 0, 1},
    {"Georgia", 1, 3, 3},
    {"Colombia", 1, 3, 4},
    {"China", 38, 27, 23},
    {"Eslovenia", 1, 1, 2},
    {"Argentina", 1, 1, 2},
    {"Hong Kong", 0, 0, 1},
    {"Francia", 11, 11, 12},        
    {"Afganistan", 0, 0, 1},        
    {"Tayikistan", 0, 0, 1},
    {"Alemania", 11, 19, 14},        
    {"Gran Bretana", 29, 17, 19},        
    {"Estados Unidos", 46, 29, 29},        
};

size_t n = sizeof(medallero) / sizeof(medallero[0]);

int cmp_paises(const void* e1, const void* e2) {
    const pais_t * p1 = e1;
    const pais_t * p2 = e2;
    
    if (p1->oro == p2->oro) {
        if (p1->plata == p2->plata) {
            if (p1->bronce == p2->bronce) {
                return strcmp(p1->nombre, p2->nombre);
            } else {
                return p2->bronce - p1->bronce;
            }
        } else {
            return p2->plata - p1->plata;
        }
    } else {
        return p2->oro - p1->oro;
    }
}

int main(void) {     
    qsort(medallero, n, sizeof(medallero[0]), cmp_paises);
    printf("------------------------------\n");
    printf("Pais              Or   Pl   Br\n");
    printf("------------------------------\n");
    size_t i;
    for (i = 0; i < n; i++) {
        printf("%-15s %4d %4d %4d\n", medallero[i].nombre, 
            medallero[i].oro, medallero[i].plata, medallero[i].bronce);
    }
    printf("------------------------------\n");
    return 0;
}