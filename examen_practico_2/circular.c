//==========================================================
// Solución al problema 2. 
//==========================================================

#include <stdio.h>
#include <stdlib.h>

struct nodo {
    struct nodo * sig;
};

typedef struct nodo nodo_t;

void suficiente_memoria(nodo_t * p) {
    if (!p) {
        fprintf(stderr, "Memoria insuficiente.\n");
        exit(1);
    }
}

nodo_t * crea_lista(int n) {
    if (n < 1) {
        return NULL;
    }
    
    nodo_t * primero;
    nodo_t * ultimo;
    
    primero = ultimo = malloc(sizeof(nodo_t));
    suficiente_memoria(primero);
    primero->sig = ultimo;
    
    int i;
    for (i = 1; i < n; i++) {
        nodo_t * nuevo = malloc(sizeof(nodo_t));
        suficiente_memoria(nuevo);
        ultimo->sig = nuevo;
        nuevo->sig = primero;
        ultimo = nuevo;
    }
    
    return primero;        
}

void destruye_lista(nodo_t * lst) {
    if (!lst) return;
    nodo_t * actual = lst->sig;
    while (actual != lst) {
        nodo_t * anterior = actual;
        actual = actual->sig;
        free(anterior);
        anterior = NULL;
    }
    free(actual);
    actual = NULL;
}

int main(void) {
    nodo_t * lst;
    
    lst = crea_lista(5);
    destruye_lista(lst);
    
    lst = crea_lista(1);
    destruye_lista(lst);
    
    lst = crea_lista(0);
    destruye_lista(lst);

    return 0;
}