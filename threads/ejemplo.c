#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* cuerpo(void* arg) {
    int i;
    int id = *(int*)arg;
    for (i = 0; i < 1000; i++) {
        printf("%d En cuerpo %d\n", id, i);
        usleep(100);
    }
    return NULL;
}

int main(void) {
    puts("Iniciando main");
    pthread_t mi_thread;
    int id_mi_thread = 0;
    
    int status = pthread_create(
        &mi_thread,
        NULL,
        cuerpo,
        &id_mi_thread
    );
    
    if (status) {
        fprintf(stderr, "No se pudo crear el thread.\n");
        exit(1);
    }
    
    int id_main = 1;
    cuerpo(&id_main);
    
    status = pthread_join(mi_thread, NULL);
    
    if (status) {
        fprintf(stderr, "No se pudo unir el thread.\n");
        exit(1);
    }
    
    puts("Terminando main");
    return 0;
}