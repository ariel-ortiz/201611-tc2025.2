#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define num_rects (1000000000)
#define width     ((1.0) / (double) (num_rects))
#define NUM_CPUS  (4)

struct limites {
    long inicio;
    long final;
    double sum;
};

void* tarea(void* arg) {
    double mid, height;
    struct limites *lims = arg;
    double sum = 0.0;
    
    for (long i = lims->inicio; i < lims->final; i++) {
        mid = (i + 0.5) * width;
        height = 4.0 / (1.0 + mid * mid);
        sum += height;
    }
    lims->sum = sum;
    return lims;
}

void error(char *mens) {
  fprintf(stderr, "%s\n", mens);
  exit(1);
} 

int main(void) {
    pthread_t t[NUM_CPUS];
    struct limites lims[NUM_CPUS];
    int s;

    for (int i = 0; i < NUM_CPUS; i++) {
        lims[i].inicio = (num_rects / NUM_CPUS) * i;
        lims[i].final = (num_rects / NUM_CPUS) * (i + 1);
        lims[i].sum = 0.0;
        s = pthread_create(&t[i], NULL, tarea, &lims[i]);
        if (s) error("No se pudo crear thread");
    }

    double sum = 0.0;
    for (int i = 0; i < NUM_CPUS; i++) {
        s = pthread_join(t[i], NULL);
        if (s) error("No se pudo unir thread");
        sum += lims[i].sum;
    }

    double area = width * sum;
    printf("Computed pi = %.20f\n", area);
    return 0;
}